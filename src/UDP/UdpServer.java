package UDP;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

import lab1.Spectrum;

public class UdpServer 
{
	 public static void main(String[] args) {
	    	DatagramSocket aSocket = null;
	      try {
	        aSocket = new DatagramSocket(9876);
	        byte[] buffer = new byte[1024];
	        while(true) {
				DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(reply);
				
				byte[] readData = reply.getData();
				Object readDataDes = null;
				try {
//					Before deserialization dont know what server got, so how to get proper name?
					
//					String pathname = "serialized_garbage.txt";
//				    Files.write(new File(pathname).toPath(), reply.getData());
				    
					readDataDes = Tools.deserialize(readData);					
					Object readDataDeserialized = Spectrum.class.cast(readDataDes);
					String name_of_file = ((Spectrum) readDataDeserialized).getParamsToSave();
				    Files.write(new File(name_of_file).toPath(), reply.getData(), null);
				    
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Reply: " + new String(readDataDes.toString()));

	        }
	      } catch (SocketException ex) {
	        Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
	      } catch (IOException ex) {
	        Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
	      } finally {
					aSocket.close();
				}
	      
	    }
}
