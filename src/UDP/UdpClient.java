package UDP;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import lab1.TimeHistory;

public class UdpClient 
{
	public static void main(String[] args) 
	{
		DatagramSocket aSocket = null;
		Scanner scanner = null;
		

		
		try {
			// args contain server hostname
			if (args.length < 1) 
			{
				System.out.println("Usage: UDPClient <server host name>");
				System.exit(-1);
			}
			
			byte[] buffer = new byte[1024];
			InetAddress aHost = InetAddress.getByName(args[0]);
			int serverPort = 9876;
			aSocket = new DatagramSocket();
			scanner = new Scanner(System.in);
			String line = "";
			while (true) 
			{
				System.out.println("Enter your message: ");
				if (scanner.hasNextLine())
					line = scanner.nextLine();
				DatagramPacket request = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
				aSocket.send(request);
				DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(reply);
				System.out.println("Reply: " + new String(reply.getData(), 0, reply.getLength()));
			}
			
			
		} catch (SocketException ex) 
		{
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
			scanner.close();
		}
		//Modyfikacja 
		try
		{
			TimeHistory packet = new TimeHistory("Machine", "DC motor 12V", ZonedDateTime.now(), (double)0.005);
			byte[] data = Tools.serialize(packet);
			InetAddress address = InetAddress.getByName("127.0.0.1");
			DatagramPacket requestTimeHistory = new DatagramPacket(data, data.length, address, 9999);
			DatagramSocket aSocketTimeHistory = new DatagramSocket();
			aSocketTimeHistory.send(requestTimeHistory);	
			byte[] bufferTimeHostory = new byte[1024];
			DatagramPacket replyTimeHistory = new DatagramPacket(bufferTimeHostory, bufferTimeHostory.length);
			aSocketTimeHistory.receive(replyTimeHistory);
			////Packed read = new Packet();
			Object read = Tools.deserialize(replyTimeHistory.getData());
		}
		catch (Exception e){} 
		
		
	}
}
